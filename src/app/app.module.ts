import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MarketModule } from './market/market.module';
import { ItemsListComponent } from './market/items-list/items-list.component';
import { MarketHeaderComponent } from './market-header/market-header.component';

@NgModule({
  declarations: [
    AppComponent,
    MarketHeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MarketModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
