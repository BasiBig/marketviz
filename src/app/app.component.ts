import { Component } from '@angular/core';
import { ItemsListComponent } from './market/items-list/items-list.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'market_viz';
}
