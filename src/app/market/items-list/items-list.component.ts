import { Component, Input } from '@angular/core';
import { Item } from '../item.model';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css'],
})
export class ItemsListComponent {
  //@Input()
  itemList : Item[] = [{label:"item1"},{label:"item2"},{label:"item3"},{label:"item5"}]
}
