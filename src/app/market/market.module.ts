import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemComponent } from './item/item.component';
import { ItemsListComponent } from './items-list/items-list.component';



@NgModule({
  declarations: [
    ItemComponent,ItemsListComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[ItemComponent,ItemsListComponent]
})
export class MarketModule { }
